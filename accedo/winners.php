<?php
$tmp_lang = (!empty($_GET['lang'])?$_GET['lang']:'EN');
$tracker_csi = (!empty($_GET['tracker_csi']) ? $_GET['tracker_csi'] : 4);
$winner_list_url = 'http://externalservices.neogames-tech.com/TickerAPI.aspx?CSI=' . $tracker_csi . '&LNG=' . $tmp_lang . '&SPD=fast&TYP=horizontal&FMT=[pn]|[won]|[ac]|[dt]&SEP=;';

if(!empty($_GET['debug'])) {
	die($winner_list_url);
}

$tmp_content = file_get_contents($winner_list_url);
$tmp_content = strip_tags($tmp_content);

$json_array = array();

$limit = ((!empty($_GET['limit']) and is_numeric($_GET['limit']))?$_GET['limit']:5);

if(!empty($tmp_content)) {
	$tmp_lines = explode(";",$tmp_content);

	if(!empty($tmp_lines) and is_array($tmp_lines)) {
		$c=0;

		$thousands_separator = '.';
		switch($tmp_lang) {
			case "EN":
				$thousands_separator = ',';break;
			case "FR":
			case "FI":
			case "SV":
				$thousands_separator = ' ';break;
		}

		foreach($tmp_lines as $r_nr => $r_row) {
			if(substr_count($r_row,"|")!=0 and $c<$limit) {
				$r_row = trim($r_row);
				$r_row_info = explode("|",$r_row);

				if(!empty($r_row_info[2])) {
					$r_row_info[4] = preg_replace("/([^0-9])/", "", $r_row_info[2]);
					$r_row_info[2] = preg_replace("/([0-9])/", "", $r_row_info[2]);
					if(is_numeric($r_row_info[4])) {
						$r_row_info[4] = number_format($r_row_info[4],0,'',$thousands_separator);
					}
				}

				$json_array[] = array(
					'name'=>$r_row_info[0],
					'price'=>$r_row_info[2],
					'date'=>$r_row_info[3],
					'amount'=>$r_row_info[4]
				);

				$c++;
			}
		}
	}
	if($c<6) {
		/*
		for($x=$c;$x<=6;$x++) {
			$json_array[] = array(
				'name'=>'-',
				'price'=>'',
				'date'=>'',
				'amount'=>''
			);
		}
		*/
	}
}

die(json_encode($json_array));