<?php
$tmp_lang = (!empty($_GET['lang'])?$_GET['lang']:'EN');
//$winner_list_url = 'http://externalservices.neogames-tech.com/TickerAPI.aspx?CSI=4&LNG='.$tmp_lang.'&SPD=fast&TYP=horizontal&FMT=[pn]|[won]|[ac]|[dt]&SEP=;';
$winner_list_url = 'https://www.gameserver1-mt.com/Gateway/Gateway.aspx?ver=1.03&rnd='.time().'&password=ydaryvuva&command=TopWinners&MinutesBack=1440';
$winner_file = 'winners.xml';

if(file_exists($winner_file)) { unlink($winner_file); }
exec('wget -O - -q "'.$winner_list_url.'" > '.$winner_file);

$json_array = array();
if(file_exists($winner_file)) {
  $tmp_content = (array) simplexml_load_file($winner_file);
  if(!empty($tmp_content) and is_array($tmp_content) and !empty($tmp_content['TopWinners'])) {
    $winner_list = (array) $tmp_content['TopWinners'];

    foreach ($winner_list['ITEM'] as $w_nr => $w_row) {
      $w_row = (array) $w_row;
      $w_row = $w_row['@attributes'];

      $thousands_separator = '.';
      switch($tmp_lang) {
        case "EN":
          $thousands_separator = ',';break;
        case "FR":
        case "FI":
        case "SE":
        case "NO":
          $thousands_separator = ' ';break;
      }

      $w_row['Win'] = number_format($w_row['Win'],0,'',$thousands_separator);

      $json_array[$w_nr] = array(
        'name'=>$w_row['ExtPlayerID'],
        'price'=>'&#8364;',
        'date'=>date("d/m H:i", strtotime($w_row['DateTime'])),
        'amount'=>$w_row['Win'],
        'game_id'=>$w_row['GameID'],
      );

    }
  }
}

echo json_encode($json_array);
