<?php
$tmp_geo_root = $_SERVER['DOCUMENT_ROOT'].'/sites/all/themes/mrmega';
include $tmp_geo_root."/GeoIP/geoip.inc";
$gi = geoip_open($tmp_geo_root."/GeoIP/GeoIP.dat",GEOIP_STANDARD);
$tmp_country_code = geoip_country_code_by_addr($gi, $_SERVER['REMOTE_ADDR']);
if(empty($tmp_country_code)) $tmp_country_code="uk";

define('DRUPAL_ROOT',$_SERVER['DOCUMENT_ROOT']);
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$new_host = '';
$default_domain = '';
$lang_array = db_query("select language,domain from {languages} order by weight");
foreach ($lang_array as $r_nr => $r_row) {
	if($r_row->language=="en") $default_domain=$r_row->domain;
	if(!empty($tmp_country_code) and $r_row->language==strtolower($tmp_country_code)) {
		$new_host = $r_row->domain;
	}
}
if(empty($new_host)) { $new_host=$default_domain; }
if(!empty($new_host)) {
	$new_host = str_replace('http://','',$new_host);
	$old_host = strtolower($_SERVER['HTTP_HOST']);
	
	if(!empty($_GET['debug'])) {
		echo 'ip: '.$_SERVER['REMOTE_ADDR'].'<br/>';
		echo 'country code: '.$tmp_country_code.'<br/>';
		echo 'new url: '.$new_host.'<br/>';
		die();
	}
	
	setcookie("mrmega_language",$new_host,strtotime("+14 days"),"/",'mrmega.com');
	if(!empty($old_host) and $new_host!=$old_host) {
		header("location: http://".$new_host.(!empty($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:''));
	} else {
		header("location: /");
	}
	die();
}
?>