<div class="box <?php echo (!empty($details->standard['desktop_or_mobile']) OR !empty($details->langauge_specefic['desktop_or_mobile']))? $details->standard['desktop_or_mobile'] : ''?><?php echo (!empty($details->classes) ? ' '.join(' ', $details->classes) : ''); ?>" id="game<?php echo $details->game_list_id.$details->entity_id; ?>">
  <?php if ((isset($details->standard['new_tag']) and $details->standard['new_tag']==1) OR (isset($details->langauge_specefic['new_tag']) and $details->langauge_specefic['new_tag']==1)): ?>
    <div class="newGame"><?php print t('New!'); ?></div>
  <?php endif; ?>
  <div class="box-image">  
    
    <?php if($details->langauge_specefic['thumb']): ?>
    <img class="lasyload" alt="" data-src="<?php echo file_create_url($details->standard['thumb']['uri']); ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
    <noscript>
      <?php echo theme('image', array('path' => $details->langauge_specefic['thumb']['uri'])); ?>
    </noscript>
    <?php elseif($details->standard['thumb']): ?>
    <img class="lasyload" alt="" data-src="<?php echo file_create_url($details->standard['thumb']['uri']); ?>" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7">
    <noscript>
      <?php echo theme('image', array('path' => $details->standard['thumb']['uri'])); ?>
    </noscript>
    
    <?php endif; ?>
    <span class="game-category"><div class="field-content"><?php echo (!empty($details->langauge_specefic['label']) ? $details->langauge_specefic['label'] : $details->standard['label']); ?></div></span>
  </div>
  <div class="box-bottom"><h3 class="box-title"><?php echo $details->title; ?></h3>
  <a class="try-game" href="#"><?php print t('Try game'); ?></a>
  <a class="more-link" onclick="<?php echo (!empty($details->standard['link']) ? 'OpenLobby(\''.$details->standard['link'].'\');' : 'return false;'); ?>" href="#">
      <?php print t('Play now'); ?>
  </a>
</div>