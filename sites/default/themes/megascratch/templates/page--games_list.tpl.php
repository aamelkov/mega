<?php
include path_to_theme().'/templates/includes/header.php';
if (drupal_load('module', 'mrmega_domains')) {
	$site = mrmega_domains_get_site();
	$tracker_sci = isset($site->field_tracker_csi_parameter[LANGUAGE_NONE][0]['value']) ? $site->field_tracker_csi_parameter[LANGUAGE_NONE][0]['value'] : 4;
}
else {
	$tracker_sci = 4;
}
?>
<?php if($messages) print $messages;?>
<div id="main">
	<div class="container cf">
 
			<div id="games-filter">
				<ul class="nav nav-horizontal">
          <li class="all"><a class="active" href="#"><?php print t('All games'); ?></a></li>
          <?php
          if (!empty($game_settings)):
            foreach ($game_settings as $game_key => $game_info):
              if ((!empty($game_info['active']) and $game_info['active']==1)):
                echo '<li class="'.$game_key.'"><a href="#">'.(!empty($game_info['name'])?$game_info['name']:'').'</a></li>';
               endif;
            endforeach;
          endif;
          ?>
				</ul>
			</div>
    
      
			<div id="games">
				<?php print render($page['content']); ?>
			</div>

  </div>
</div><?php
include path_to_theme().'/templates/includes/footer.php';
