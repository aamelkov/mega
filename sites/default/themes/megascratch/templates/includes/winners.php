<div id="winners" class="box">
	<?php
	$split_title = explode(" ",$winner_list_titel,2);
	?>
	<h3 class="box-title">
		<em><?php echo (!empty($split_title[0])?$split_title[0]:''); ?></em>
		<?php echo (!empty($split_title[1])?$split_title[1]:''); ?>
	</h3>
	<ul id="winners-list"></ul>
	<script type="text/javascript">
	var slideSec = 0.5;
					var waitSec = 2;
					var currSlideIndex = 0;
					var max_items = <?php echo ($selected_site==11?'3':'5'); ?>;
					var items = max_items;

					function startFade() {
							if(currSlideIndex != items) {
									var first1 = jQuery('#winners-list li:eq('+currSlideIndex+') div.active');
									var next1 = jQuery('#winners-list li:eq('+currSlideIndex+') div:eq('+(first1.index() + 1)+')');

									if(first1.length > 0) {
											if(next1.length == 0) {
													next1 = jQuery('#winners-list li:eq('+currSlideIndex+') div:eq(0)');
											}

											if(next1.length > 0) {
													next1.css({ top: '-68px', display: 'block' });
													first1.removeClass('active');
													next1.addClass('active');

													next1.animate({
															top: '5px'
													}, (slideSec*1000), function () {
															currSlideIndex++;

															startFade();
													});

													first1.animate({
															top: '68px'
													}, (slideSec*1000), function () {});
											}
									}
							} else {
									currSlideIndex = 0;
									setTimeout('startFade()', (waitSec*1000));
							}
					}

					jQuery(document).ready(function() {
							jQuery.ajax({
									type: 'GET',
									dataType: 'json',
									cache: false,
									url: '/accedo/winners.php',
									data: {
											lang: '<?php echo strtoupper($language_code); ?>',
											limit: 15,
											tracker_csi: '<?php echo $tracker_sci;?>'
									},
									success: function (data) {
											var index = 0;
											var minus = parseInt(Object.keys(data).length/2);
											if (items > max_items) minus = max_items;
											items = minus;

											jQuery.each(data, function(i, item) {
													index += 1;

													var tmpPriceLabel = "";

													if(item.price.length>0 && item.price=="kr") {
															tmpPriceLabel = item.amount+" "+item.price+".";
													} else {
															tmpPriceLabel = item.price+" "+item.amount;
													}

													if(index <= items) {
															jQuery("#winners-list").append('<li><div><span class="how-much">'+tmpPriceLabel+'</span><span class="who">'+item.name+'</span><span class="when">('+item.date+')</span></div></li>');
													} else {
															var currCount = index - minus;

															var obj = jQuery('#winners-list li:eq('+(currCount - 1)+')');

															if(obj.length > 0) {
																	obj.append('<div><span class="how-much">'+tmpPriceLabel+'</span><span class="who">'+item.name+'</span><span class="when">('+item.date+')</span></div>');
															}

															if(currCount == items) {
																	minus += items;
															}
													}
											});

											jQuery('#winners-list li').each(function (index, item) {
													$obj = jQuery(item).find('div:eq(0)');
													setTimeout(function () {
															var obj = jQuery('#winners-list li:eq('+index+')').find('div:eq(0)');
															obj.addClass('active');
															obj.fadeIn(300);
															if (index + 1 == items) { //(index + 2) == items
																	startFade();
															}
													}, (200 * (index + 1)));
											});
									}
							});
	});
	  </script>
	</div>