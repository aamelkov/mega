<div id="footer">
	<div class="menu-bottom-wrapper clearfix">
			<?php
			  print render($page['menus_bottom']);
			?>
	</div>
	<!--rev353 revert payment cards-->
  <?php print render($page['footer']); ?>

</div>
<div id="colophon">
  <div class="footer-logo">
    <img src="<?php print base_path() . path_to_theme(); ?>/img/footer-logo.png" alt="Megascratch"/>
  </div>
	<div class="container">
		<?php print (!empty($page['colophon']) ? str_replace('[year]',format_date(REQUEST_TIME,'custom','Y'),render($page['colophon'])) : ''); ?>
	</div>
</div>
</div>
