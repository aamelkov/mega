<?php
$tmp_game_id = (!empty($output) ? preg_replace('/([^0-9])/','',$output) : 0);

if(empty($output) or (!empty($tmp_game_id) and is_numeric($tmp_game_id))):
  $output = _mrmegamod_game_link_format($tmp_game_id);
endif;
echo '<a href="' . $output . '" target="_blank" data-game-id="'.$tmp_game_id.'">' . $output . '</a>';

