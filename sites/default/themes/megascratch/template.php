<?php

/**
 * Megascratch page prossess
 *
 * @param array $variables
 **/
function megascratch_preprocess_page(&$variables) {
	$node = menu_get_object();

	if (!empty($node)) {
		if (isset($variables['node']->type)) {
			$variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
		}
		if(isset($variables['node']->type) and $variables['node']->type == "subpage") {
			$variables['theme_hook_suggestions'][] = 'page--subpage';
		}

		$variables = _mrmegamod_variables_update($variables);
	}

	if ($variables['user']->uid == 0 && arg(0) == 'user' && (arg(1) == '' || arg(1) == 'login')) {
		$variables['theme_hook_suggestions'][] = 'page__login';
	}
// Webmaster verification

 // Setup Google Webmasters Verification Meta Tag
  $google_webmasters_verification = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'google-site-verification',
      // REPLACE THIS CODE WITH THE ONE GOOGLE SUPPLIED YOU WITH
      'content' => 'gHA8-HJUoFKp8onG9e1POv9cRVkbFmNcXvUyYvSJnPg',
    )
  );
  
  // Add Google Webmasters Verification Meta Tag to head
  drupal_add_html_head($google_webmasters_verification, 'google_webmasters_verification');

}

/**
 * Get attributes and load from variables
 *
 * @param array $variables
 **/
function megascratch_links__locale_block($variables) {
	//$valrables['attributes']['class'][] = 'nav';
	//$variables['attributes']['id'][] = 'nav-lang';
	//return theme('links', $variables);

	$items = array();
	foreach($variables['links'] as $lang => $info) {
    if(!empty($info['language']->domain)) {
      $info['language']->domain = str_replace('en.','',$info['language']->domain);
      $info['language']->domain = str_replace('.www.','.',$info['language']->domain);
    }

		if (isset($info['href'])) {
			$itetm = array();
			$itetm['class'] = array($lang);
			$options = array(
				'attributes' => array('class' => array('language-link')),
				'language' => $info['language'],
				'html' => TRUE,
			);
			if(!empty($info['active'])){
				$itetm['class'][] = 'active';
				$options['attributes']['class'][] = 'active';
			}

//      $info['href'] = '';

			$itetm['data'] = l($info['language']->native, $info['href'], $options);

			if(!empty($info['active'])) array_unshift($items, $itetm);
			else $items[] = $itetm;
		}
  }
	return theme_item_list(array('items' => $items, 'title' => '', 'type'  => 'ul', 'attributes' => array('class' => array('nav', 'language-switcher-locale-url'), 'id' => array('nav-lang'))));
}

/**
 * Function to grap the language code from page array
 *
 * @param array $page_array
 * @return string $data
 **/
function get_langcode($page_array) {
	global $language;
	if(!empty($language->language)) {
		return $language->language;
	} else {
		$host = $_SERVER['HTTP_HOST'];
		if(!empty($host)) {
			$host = substr($host,0,strpos($host,"."));
		}
		return $host;
	}
}

/**
 * Implements theme_link.
 */
function megascratch_link($variables) {
  return _mrmegamod_links_format($variables);
}

/**
 * Alter canonical so to leave only one on the page.
 */
function megascratch_html_head_alter(&$head_elements) {
  _megascratch_alter_header_tags($head_elements);
}
