<?php
$page_title = (!empty($node->title)?$node->title:'');

$headline = (!empty($node->field_sub_title[LANGUAGE_NONE][0]['value']) ? '<em>'.$node->field_sub_title[LANGUAGE_NONE][0]['value'].'</em>' : '');
$headline = '<h1>'.(!empty($node->field_top_title[LANGUAGE_NONE][0]['value']) ? $headline.$node->field_top_title[LANGUAGE_NONE][0]['value'] : '').'</h1>';

include path_to_theme().'/templates/includes/header.php';

$page_content = render($page['content']);
_mrmegamod_add_info_callback($page_content);

?>
<div id="main">
	<div class="container cf">
		<div id="sub-content">
			<?php if ($node-> field_exclude_headlines['und'][0]['value']!='1'):?>
				<?php echo $headline;?>
			<?php endif; ?>
			<?php echo $page_content;?>
		</div>
	</div>
</div>
<?php
include path_to_theme().'/templates/includes/footer.php';
