<?php
if(!empty($page) and is_array($page)) $language_code = get_langcode($page);
if(empty($language_code)) $language_code='en';

$ENV = '';
$AFI = 47;
$LNG = "ENG";
$CUR = "EUR";
$AR = (!empty($_GET['AR'])?$_GET['AR']:'');
$PAR = (!empty($_GET['PAR'])?$_GET['PAR']:'');
switch(strtolower($language_code)) {
	case "fr": $LNG = "FRE";break;
	case "es": $LNG = "SPA";break;
	case "de": $LNG = "GER";break;
	case "it": $LNG = "ITA";break;
	//case "nl": $LNG = "DUT";break;
	case "pt": $LNG = "POR";break;
	case "br": $LNG = "BRA";$CUR="BRL";break;
	case "sv": $LNG = "SWE";$CUR="SEK";break;
	case "no": $LNG = "NOR";$CUR="NOK";break;
	case "fi": $LNG = "FIN";break;
	case "el": $LNG = "GRE";break;
}
if(!empty($_SERVER['HTTP_HOST'])) {
	if(substr_count($_SERVER['HTTP_HOST'],'.dev.')!=0) {
		$ENV = 'dev';
	} elseif(substr_count($_SERVER['HTTP_HOST'],'.st.')!=0) {
		$ENV = 'st';
	} elseif(substr_count($_SERVER['HTTP_HOST'],'.qa.')!=0) {
		$ENV = 'qa';
	}
}
$iframe_url = 'http://info'.(!empty($ENV)?'.'.$ENV:'').'.mrmega.com/visit.aspx'.
'?LNG='.$LNG.
'&CUR='.$CUR.
'&AFI='.$AFI.
'&AR='.$AR.
'&PAR='.$PAR;

$qString = '';
if(!empty($_SERVER['REQUEST_URI'])) {
  $tmp = parse_url($_SERVER['REQUEST_URI']);
  if(!empty($tmp['query'])) {
    $qString = $tmp['query'];
  }
}

//http://info.mrmega.com/visit.aspx?LR=1&amp;LNG=ENG&amp;CUR=EUR&amp;AFI=47&amp;AR=&amp;PAR=&amp;GID=&amp;PRD=

?><script type="text/javascript"><!--//
var languageCode = "<?php echo $language_code; ?>";
var domainName = "<?php echo (!empty($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:'www.mrmega.com'); ?>";
var qsString = "<?php echo $qString; ?>";

//--></script>

<div id="page-wrapper">
<div id="header">
  <div class="container header-<?php echo $language_code; ?> clearfix">
	  <div class="top-header clearfix">

      <div class="top-header-items clearfix">
        <div class="contact-info">
          <?php
            $langcode = $GLOBALS['language']->language;
            $site_phone = i18n_variable_get('mrmega_phone', $langcode);
            $output = '';
            $phones = explode(",", $site_phone);
            if(!empty($phones) && is_array($phones)) {
							foreach($phones as $ph) {
                $output .= '<div class="phslide">'.$ph.'<i class="fa fa-phone"></i></div>';
							}
            }
            $block = module_invoke('menu', 'block_view', 'menu-chat-and-contact---mrmega');
            print render($block['content']);
          ?>
        </div>
        <div class="phone-wrap desktop-show"><?php print $output; ?></div>
        <div class="phone-wrap mob-show"><a href="tel:<?php print !empty($site_phone) ? $site_phone : '+45 8088 7130'; ?>"><i class="fa fa-phone"></i></a></div>
        <div class="language-block">
          <?php
          $block = module_invoke('locale', 'block_view', 'language');
          print render($block['content']);
          ?>
        </div>
      </div>
      <div class="mob-hd clearfix">
        <span class="mobile-menu-icon"><i class="fa fa-2x fa-bars"></i></span>
        <?php if ($logo): ?>
             <a href="/" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img class="desktop-show" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
              <img class="mob-show" src="<?php print base_path() . path_to_theme(); ?>/img/mob-logo.png" alt="<?php print t('Home'); ?>"/>
             </a>
        <?php endif; ?>
      </div>
			<div id="access" class="cf">
			  <?php
        $tmp_header = render($page['header']);
        _mrmegamod_add_info_callback($tmp_header);

			  print $tmp_header;
        ?>
			</div>
			<!-- /#access -->
		</div>

	</div>

</div>
