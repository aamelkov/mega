<?php
$host = (!empty($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST']:'');
$base_path = 'http://' . $host;
?>
<div id="footer">
    <div class="menu-bottom-wrapper clearfix">
      <?php
      $tmp_footer_menu = render($page['menus_bottom']);
      $tmp_footer_menu = str_replace('"/sites/', '"'.$base_path.'/sites/', $tmp_footer_menu);
      print $tmp_footer_menu;
      ?>
    </div>
    <?php
    $tmp_footer_content = render($page['footer']);
    $tmp_footer_content = str_replace('"/sites/', '"'.$base_path.'/sites/', $tmp_footer_content);
    print $tmp_footer_content;
    ?>
</div>

<div id="colophon">
  <div class="footer-logo">
    <img src="<?php print $base_path . '/' . path_to_theme(); ?>/img/footer-logo.png" alt="MrMega"/>
  </div>
  <div class="container">
    <?php print (!empty($page['colophon']) ? str_replace('[year]', format_date(REQUEST_TIME, 'custom', 'Y'), render($page['colophon'])) : ''); ?>
  </div>
</div>
