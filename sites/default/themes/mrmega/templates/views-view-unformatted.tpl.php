<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<ul id="gameList" class="nav image-grid">
<?php foreach ($rows as $id => $row): ?>
  <li data-id="id-<?php echo $id; ?>" class="<?php print $classes_array[$id]; ?>">
    <div class="<?php print $classes_array[$id]; ?>">
	<?php print $row; ?>
    </div>
  </li>
<?php endforeach; ?>
</ul>