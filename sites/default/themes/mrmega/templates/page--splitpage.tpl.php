<?php
if(defined('SHOWONLY') and SHOWONLY=='header'):
  include path_to_theme().'/templates/includes/header.php';

?>
<div id="main">
	<div class="container cf">
		<div id="sub-content">
<?php endif; ?>

<?php if(defined('SHOWONLY') and SHOWONLY=='footer'): ?>
		</div>
	</div>
</div>
<?php
include path_to_theme().'/templates/includes/footer.php';
endif;
