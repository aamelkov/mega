<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

?>
<?php if ($view->name != 'top_slider'): ?>

<div class="box <?php echo strtolower($fields['field_term']->content); ?>">
	<?php
	$display_new_game = $fields['field_display_new_game_tag']->content;
	if(!empty($display_new_game)) $display_new_game=strip_tags($display_new_game);
	$display_new_game = $display_new_game==1?1:0;

	/*
	$post_time = $fields['created']->content;
	if(!empty($post_time)) $post_time=strip_tags($post_time);
	if(is_numeric($post_time)) {
		//$post_time = (time()-$post_time/86400);
		$post_time = ((time()-$post_time)/86400);
	}
	*/
	if($display_new_game==1) {
		?><div class="newGame">&#160;</div><?php
	}
	$tmp_link = (!empty($fields['field_game_link']->content)?$fields['field_game_link']->content:'');
	if(!empty($tmp_link)) {
		$tmp_link .= (substr_count($tmp_link,"?")!=0?'&amp;':'?');
		if(substr_count($tmp_link,"PAR=")!=0) { $tmp_link=str_replace('PAR=','oldPAR=',$tmp_link); }
		if(substr_count($tmp_link,"AR=")!=0) { $tmp_link=str_replace('AR=','oldAR=',$tmp_link); }
		$tmp_link .= 'AR='.(!empty($_GET['AR'])?$_GET['AR']:'');
		$tmp_link .= '&amp;PAR='.(!empty($_GET['PAR'])?$_GET['PAR']:'');
	}
	?>
	<div class="box-content">
		<h3 class="box-title"><?php print($fields['title']->content); ?></h3>
		<?php print($fields['field_game_thumb']->content);?>
		<?php
		$game_label = (!empty($fields['field_term']->content)?$fields['field_term']->content:'');
		$game_label = ((!empty($fields['field_game_label']->content) and strip_tags($fields['field_game_label']->content)!="")?$fields['field_game_label']->content:$game_label);
		?>
		<span class="game-category"><?php print($game_label);?></span>
	</div>
	<a class="more-link" onclick="OpenLobby('<?php print($tmp_link); ?>');" href="#"><?php echo ((!empty($fields['field_button_text']->content) and strip_tags($fields['field_button_text']->content)!="")?$fields['field_button_text']->content:'Play now') ?></a>
	<div class="popover">
		<h3 class="popover-title"><?php print($fields['title']->content);?></h3>
		<span class="popoverContent">
			<div class="view-carousel"></div>
			<div class="popover-carousel"><?php print($fields['field_game_images']->content);?></div>
			<?php
			$tmp_content = $fields['body']->content;
			if(strlen($tmp_content)>260) {
				$tmp_content = substr($tmp_content,0,260).'...';
			}
			print($tmp_content);
			?>
			<blockquote>
			<?php
			$tmp_content = $fields['field_quotation']->content;
			if(strlen($tmp_content)>60) {
				$tmp_content = substr($tmp_content,0,60).'...';
			}

			print($tmp_content);
			?>
			</blockquote>
		</span>
		<cite><?php /* print($fields['field_quotation_author']->content); */ ?></cite>
	</div>
</div>
<?php else: ?>
	<?php foreach ($fields as $id => $field): ?>
	  <?php if (!empty($field->separator)): ?>
	    <?php print $field->separator; ?>
	  <?php endif; ?>

	  <?php print $field->wrapper_prefix; ?>
	    <?php print $field->label_html; ?>
	    <?php print $field->content; ?>
	  <?php print $field->wrapper_suffix; ?>
	<?php endforeach; ?>
<?php endif; ?>
