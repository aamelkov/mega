<?php if(!defined('SHOWONLY')): ?>
<!DOCTYPE html">
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php
  $head = str_replace('en.www.', 'www.', $head);
  print $head;
  ?>
  <title><?php print (defined('PAGE_TITLE') ? PAGE_TITLE : $head_title); ?></title>

  <script src="//use.typekit.net/thz7kmh.js"></script>
  <script>try{Typekit.load();}catch(e){}</script>

  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="<?php print base_path() . path_to_theme(); ?>/js/matchMedia.addListener.js"></script>
  <script src="<?php print base_path() . path_to_theme(); ?>/js/matchMedia.js"></script>
	<script>
		(function(){function LowerThanAndroidVersion(t){var e=navigator.userAgent,n=e.indexOf("Android");if(n>=0){var i=e.substring(n+8,e.length),o=i.substring(0,i.indexOf(";"));return t>o?!0:!1}}var setViewport=function(){matchMedia("only screen and (min-width: 737px)").matches?document.getElementById("viewport").setAttribute("content","width=1020"):document.getElementById("viewport").setAttribute("content","width=device-width, initial-scale=1.0")};LowerThanAndroidVersion("3")?window.onload=function(){setViewport()}:setViewport(),window.onresize=function(){setViewport()};	})();
	</script>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
<?php else: ?>
<!--// Content - START //-->
  <?php print $page; ?>
<!--// Content - END //-->
<?php endif; ?>
