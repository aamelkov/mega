<?php
include path_to_theme().'/templates/includes/header.php';
if (drupal_load('module', 'mrmega_domains')) {
	$site = mrmega_domains_get_site();
	$tracker_sci = isset($site->field_tracker_csi_parameter[LANGUAGE_NONE][0]['value']) ? $site->field_tracker_csi_parameter[LANGUAGE_NONE][0]['value'] : 4;
}
else {
	$tracker_sci = 4;
}

?>
<div id="main">
	<div class="container cf">

    <div class="jackpot">
    <?php $banner = variable_get('mrmega_game_lang'.$selected_language.'_bannerimage',''); ?>
    <?php if(!empty($banner)): ?>
      <?php $imgpath = file_load($banner)->uri; ?>
      <img class="desktop-show" src="<?php print file_create_url($imgpath); ?>" alt="Jackpot"/>
    <?php endif; ?>
      <img class="mob-show" src="<?php print base_path() . path_to_theme(); ?>/img/mob-jackpot.png" alt="Jackpot"/>
      <div class="jackpot-right clearfix">
        <div class="jp amount">
            <span class="value"><?php print (!empty($jackpot['amount']) ? $jackpot['amount'] : 0); ?></span><span class="value2">,</span><span class="value3">10</span>
            <span class="currency"><?php print variable_get('mrmega_game_lang'.$selected_language.'_cursymb',''); ?></span>
        </div>

        <?php
        if(!empty($jackpot['url'])):
          $link = l(t('Play now'), $jackpot['url'], array(
            'attributes' => array(
              'target' => (!empty($jackpot['target']) ? $jackpot['target'] : ''),
              'class' => array('more-link', 'slide'),
            ),
          ));
          _mrmegamod_add_info_callback($link);

          echo $link;
        endif;
        ?>
       </div>


      <script>
      jQuery(document).ready(function($) {
        var jackpot = <?php print (!empty($jackpot['amount']) ? $jackpot['amount'] : 0); ?>;
        var jackpot_final = jackpot;

        function formatNum(num) {
          var parts = String( num ).replace(/[^\d.]-/g,'').split('.');
          var arr = parts[0].split('').reverse();

          var str = '';

          for( var i = 0; arr.length; i++ ) {
            if( i && i%3 == 0 && arr[0] != '-' ) {
              str  = '.' + str ;
            }

            str = arr.shift() + str ;
          }

          return str + ( parts[1] ? ','+parts[1] : '' );
        }

        jQuery({
          someValue: jackpot_final}).animate({someValue: 2000000}, {
            duration: 6000000000,
            easing:'linear',
            step: function() {
              $('.value').text(formatNum(Math.round(this.someValue)));
            },
            complete:function(){
              $('.value').text(formatNum(Math.round(this.someValue)));
            }
          }
        );

        jQuery('.value3').counter({
            autoStart: true,
            duration: 4000,
            countFrom: 11,
            countTo: 99,
            runOnce: false,
            placeholder: "",
            easing: "linear",
            onStart: function() {},
            onComplete: function() {jQuery('.value3').counter("start");}
        });
    });
    </script>

    </div>

    <div id="games">
      <?php print render($page['content']); ?>
    </div>

    <div class="front-bottom-boxes grid clearfix">
      <?php if(!empty($on_road)): ?>
        <div class="unit half">

          <div class="box on-road">
            <?php print $on_road; ?>
          </div>
         </div>
      <?php endif; ?>

         <div class="unit one-quarter">
       <?php if(!empty($help_html)): ?>
          <div class="box help">
            <?php print $help_html; ?>
          </div>
       <?php endif; ?>
       <?php if(!empty($play_now)): ?>
          <div class="box pl-now">
            <?php print $play_now; ?>
          </div>
         </div>
      <?php endif; ?>
        <div class="unit one-quarter">
         <?php include path_to_theme().'/templates/includes/winners.php'; ?>
        </div>
     </div>
   </div>
	</div>
</div><?php
include path_to_theme().'/templates/includes/footer.php';

