<?php

/**
 * MrMega page prossess
 *
 * @param array $variables
 **/
function mrmega_preprocess_page(&$variables) {
	if ($node = menu_get_object()) {
		if (isset($variables['node']->type)) {
			$variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
		}
		if($variables['node']->type=="subpage") {
			//$variables['theme_hook_suggestions'][] = 'page--subpage';
		}
	}
  elseif(arg(0)=='services' and arg(1)=='WebsiteHeaderProvider.aspx') {
    $variables['theme_hook_suggestions'] = array('page__splitpage_header');
  }
  elseif(arg(0)=='services' and arg(1)=='WebsiteFooterProvider.aspx') {
    $variables['theme_hook_suggestions'] = array('page__splitpage_footer');
  }

	if ($node = menu_get_object() or drupal_is_front_page() or arg(0)=='services') {
		$variables = _mrmegamod_variables_update($variables);
	}

	if ($variables['user']->uid == 0 && arg(0) == 'user' && (arg(1) == '' || arg(1) == 'login')) {
		$variables['theme_hook_suggestions'][] = 'page__login';
	}
// Webmaster verification

 // Setup Google Webmasters Verification Meta Tag
  $google_webmasters_verification = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'google-site-verification',
      // REPLACE THIS CODE WITH THE ONE GOOGLE SUPPLIED YOU WITH
      'content' => 'wfbZ-QYX8q6DvNp6NnhkVbe7dhXe71OxsRZ1MX5Aj0M',
    )
  );

  // Add Google Webmasters Verification Meta Tag to head
  drupal_add_html_head($google_webmasters_verification, 'google_webmasters_verification');
// Pinterest verification

 // Setup Pinterest Verification Meta Tag
  $pinterest_verification = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'p:domain_verify',
      // REPLACE THIS CODE WITH THE ONE PINTEREST SUPPLIED YOU WITH
      'content' => '81bd7bd9490159816af9f8298c0af3c7',
    )
  );

  // Add Pinterest Verification Meta Tag to head
  drupal_add_html_head($pinterest_verification, 'pinterest_verification');
}

/**
 * Get attributes and load from variables
 *
 * @param array $variables
 **/
function mrmega_links__locale_block($variables) {
	//$valrables['attributes']['class'][] = 'nav';
	//$variables['attributes']['id'][] = 'nav-lang';
	//return theme('links', $variables);

	$items = array();
	foreach($variables['links'] as $lang => $info) {
    if(!empty($info['language']->domain)) {
      $info['language']->domain = str_replace('www.','',$info['language']->domain);
    }

		if (isset($info['href'])) {
			$itetm = array();
			$itetm['class'] = array($lang);
			$options = array(
				'attributes' => array('class' => array('language-link')),
				'language' => $info['language'],
				'html' => TRUE,
			);
			if(!empty($info['active'])){
				$itetm['class'][] = 'active';
				$options['attributes']['class'][] = 'active';
			}
			$itetm['data'] = l($info['language']->native, $info['href'], $options);

			if(!empty($info['active'])) array_unshift($items, $itetm);
			else $items[] = $itetm;
		}
  }

	return theme_item_list(array(
    'items' => $items,
    'title' => '',
    'type'  => 'ul',
    'attributes' => array(
      'class' => array(
        'nav', 'language-switcher-locale-url'
      ),
      'id' => array('nav-lang'),
    ),
  ));
}

/**
 * Function to grap the language code from page array
 *
 * @param array $page_array
 * @return string $data
 **/
function get_langcode($page_array) {
	if(!empty($language->language)) {
		return $language->language;
	}
  else {
		$host = $_SERVER['HTTP_HOST'];
		if (!empty($host)) {
			$host = substr($host,0,strpos($host,"."));
		}
		return $host;
	}
}

/**
 * Implements theme_link.
 */
function mrmega_link($variables) {
  return _mrmegamod_links_format($variables);
}
