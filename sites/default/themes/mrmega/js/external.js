(function ($) {

  $("#access .menu li").each(function(){
    if($(this).children('a').hasClass('active')) {
      $(this).addClass('active');
    }
  });

  $("#footer .menu li").each(function(){
    if($(this).children('a').hasClass('active')) {
        $(this).addClass('active');
    }
  });

  $("#nav-lang li a").click(function(event){
    var tmpLocation = window.location.href;
    var isExternal = substr_count($("#access").attr("class"),"external_site");
    tmpLocation = tmpLocation.replace("http://","");
    tmpLocationArray = tmpLocation.split('/');
    if(tmpLocationArray.length>1) {
      tmpLocationArray[0] = "";
      tmpLocation = $(this).attr("href"); //+tmpLocationArray.join('/')
      if(tmpLocation=="/" || tmpLocation=="//") { tmpLocation=""; }
      $(this).attr("href",tmpLocation);
    }
  });

}(jQuery));






