(function ($) {
  Drupal.behaviors.mrmega = {
    attach: function (context, settings) {
      /* Passing variables from PHP  (Drupal.settings.basePath , Drupal.settings.pathToTheme ) */

      $('html').removeClass('no-js');

      var divs = $('#popularList').find('li');
      /* Javascript media queries */
      if (matchMedia('only screen and (max-width: 736px)').matches) {
          $('#access .menu > li.last a').appendTo('.mob-hd').addClass('open-account');
          $('.mobile-menu-icon').click(function () {
            $(this).toggleClass('open-icon');
            $('#access .menu').toggleClass('open');
          });
			 }
      else {

      }

       for(var i = 0; i < divs.length; i+=4) {
   			divs.slice(i, i+4).wrapAll('<div class="frame"><div class="frame-inner"></div></div>');
       }
//       $('.phone-wrap').cycle({
//           timeout: 10000,
//           slides: '.phslide',
//           swipe: true
//       });
       $('#popularList').cycle({
          timeout: 5000,
          slides: '.frame',
          prev: '.slider-controls .fa-chevron-left',
          next: '.slider-controls .fa-chevron-right',
          pager: '.slider-controls .pager-cy',
          swipe: true
      });

      $('.view-top-slider ul').cycle({
          timeout: 8000,
          slides: 'li',
          prev: '.slider-controls-sl .fa-chevron-left',
          next: '.slider-controls-sl .fa-chevron-right',
          pager: '.slider-controls-sl .pager-cy-sl',
          swipe: true
      });





//      window.onbeforeunload = function(e) {
//         $.cookie('jackpotSum', $('.jackpot .amount').text());
//       };
//         $("#PCJSF_ScriptContainer + div").addClass('hide');
//          $("#PCJSF_ScriptContainer + div + div").addClass('show');
//          $(".contact-info ul li:first-child a").click(function (e) {
//         e.preventDefault();
//         $("#PCJSF_ScriptContainer + div").toggleClass('show');
//         $("#PCJSF_ScriptContainer + div + div").toggleClass('hide');
//        });

        $("#access .menu li").each(function(){
          if($(this).children('a').hasClass('active')) {
            $(this).addClass('active');
          }
        });

        $("#footer .menu li").each(function(){
          if($(this).children('a').hasClass('active')) {
              $(this).addClass('active');
          }
        });


        $("#nav-lang li a").click(function(event){
          var tmpLocation = window.location.href;
          var isExternal = substr_count($("#access").attr("class"),"external_site");
          tmpLocation = tmpLocation.replace("http://","");
          tmpLocationArray = tmpLocation.split('/');
          if(tmpLocationArray.length>1) {
            tmpLocationArray[0] = "";
            tmpLocation = $(this).attr("href"); //+tmpLocationArray.join('/')
            if(tmpLocation=="/" || tmpLocation=="//") { tmpLocation=""; }
            $(this).attr("href",tmpLocation);
          }
        });

        function inView(elem){
          var viewTop = $(window).scrollTop();
          var viewBottom = viewTop + $(window).height();
          var elemTop = $(elem).offset().top;
          return ((elemTop <= viewBottom) && (elemTop >= viewTop));
        }
        function lazyLoad() {
          var imgLazy = $('.lasyload');
          imgLazy.each(function () {
            if (inView($(this))) {
              var relSrc = $(this).attr('data-src');
              $(this).attr('src', relSrc);
            }
          })
        }
        lazyLoad();
        $(window).scroll(function() {
          lazyLoad();
        });

        $('#games-filter ul li a').on('click', function(event){
          event.preventDefault();
          $('#games-filter ul li a').removeClass('active');
          $(this).addClass('active');
          var filterClass = $(this).parent('li').attr('class');

          $('#gameList').find('li').each(function() {
            if ($(this).hasClass(filterClass)) {
              $(this).css('display', 'block');
            }
            else {
              $(this).css('display', 'none');
            }
          })
        });

        $links = $('a[target="_blank"]');
        if($links.length > 0) {
          $links.each(function (index, item) {
            $this = $(this);
          //  $this.attr('target', '_self')

            $this.click(function (e) {
              e.preventDefault();
              $this = $(this);
              OpenLobby($this.attr('href'));
            });
          });
        }
        $links = $('area[target="_blank"]');
        if($links.length > 0) {
          $links.each(function (index, item) {
            $this = $(this);
            $this.attr('target', '')
            $this.click(function (e) {
              e.preventDefault();
              $this = $(this);
              OpenLobby($this.attr('href'));
            });
          });
        }


        function substr_count(string,substring,start,length) {
          var c = 0;
          if(start) { string = string.substr(start); }
          if(length) { string = string.substr(0,length); }
          for (var i=0;i<string.length;i++) {
            if(substring == string.substr(i,substring.length)) c++;
          }
          return c;
        }

    }
  }
}(jQuery));




