/**
 * @file
 * The main js file of Cookie Dsclamer.
 */
var getScriptURL = (function() {
    var scripts = document.getElementsByTagName('script');
    var index = scripts.length - 1;
    var myScript = scripts[index];
    return function() { return myScript.src; };
})();

var scriptURL = getScriptURL();

scriptURL = scriptURL.split('#');

if(scriptURL.length == 2) {
    scriptURL = '#'+scriptURL[1];
} else {
    scriptURL = window.location.hash;
}

var HashSearch = new function () {
    var params;
    
    this.remove = function (key, value) {
       delete params[key];
       this.push();
    };
    
    this.get = function (key, value) {
        return params[key];
    };
    
    this.keyExists = function (key) {
        return params.hasOwnProperty(key);
    };
    
    (this.load = function () {
        params = {}
        var hashStr = scriptURL, hashArray, keyVal
        hashStr = hashStr.substring(1, hashStr.length);
        hashArray = hashStr.split('&');
        
        for(var i = 0; i < hashArray.length; i++) {
            keyVal = hashArray[i].split('=');
            params[unescape(keyVal[0])] = (typeof keyVal[1] != "undefined") ? unescape(keyVal[1]) : keyVal[1];
        }
    })();
}

;(function (d, w, undefined) {
    var C;
    
    var supports = (function() {
        var div = document.createElement('div'),
        vendors = 'Khtml Ms O Moz Webkit'.split(' '),
        len = vendors.length;
        
        return function(prop) {
            if ( prop in div.style ) return true;
            
            prop = prop.replace(/^[a-z]/, function(val) {
                return val.toUpperCase();
            });
            
            while(len--) {
                if ( vendors[len] + prop in div.style ) {
                    return true;
                } 
            }
            return false;
        };
    })();
    
    var ie = (function(){
    
       var undef,
           v = 3,
           div = document.createElement('div'),
           all = div.getElementsByTagName('i');
       
       while (
           div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
           all[0]
       );
       
       return v > 4 ? v : undef;
       
   }());
    
    function Cookie() {
        this.targetHeight = (ie ? 495 : 456);
        this.stepCount = 8;
        this.targetSteps = [];
        this.targetStepsFirst = [];
        this.targetStepsLast = [];
        this.overAllStep = [];
        this.currentStep = 0;
        this.speed = 24;
        this.animateCon = null;
        this.supportTransition = null;
        this.cookieStr = d.cookie;
        this.firstStepCount = (ie ? 10 : 9);
        
        this.userAgent = navigator.userAgent.toLowerCase();
        
        this.device = {
            ipad: false,
            iphone: false,
            android: false
        };
        
        this.moreShown = false;
        
        this.today = new Date();
        this.expire = new Date(this.today.getTime() + (361 * 24 * 60 * 60 * 1000));
    }
    
    Cookie.prototype.DOMload = function () {
        if(this.getCookie('cookie-allow-site') == null && window.self === window.top) {
            this.detectDevice();
            
            if((this.device.android || this.device.iphone) && this.targetHeight > w.innerHeight) {
                this.targetHeight = w.innerHeight;
            }
            
            this.checkSupport();
            
            if(!this.supportTransition) {
                var tmp = this.targetHeight;
                
                while(tmp > 0) {
                    tmp -= this.stepCount;
                    
                    if(tmp >= 0) this.targetSteps.push(tmp);
                    
                    if(tmp > 0 && (tmp - this.stepCount) <= 0) {
                        this.targetSteps.push(0);
                    }
                }
                
                this.targetSteps.reverse();
                
                for(var i = 0; i < this.targetSteps.length; i++) {
                    if(i < this.firstStepCount) {
                        this.targetStepsFirst.push(this.targetSteps[i]);
                    } else {
                        this.targetStepsLast.push(this.targetSteps[i]);
                    }
                    
                    this.overAllStep.push(this.targetSteps[i]);
                }
                
                this.targetSteps = this.targetStepsFirst;
            }
            
            this.buildHTML();
            
            var thiz = this;
            
            if(this.supportTransition) {
                setTimeout(function () {
                    thiz.animateCon.style.height = (thiz.firstStepCount * thiz.stepCount)+'px';
                }, 500);
            } else {
                setTimeout(function () {
                    thiz.renderStep();
                }, 500);
            }
        }
    }
    
    Cookie.prototype.detectDevice = function () {
        this.device.ipad    = this.userAgent.match(/ipad/i);
        this.device.iphone  = (this.userAgent.match(/iphone/i) || this.userAgent.match(/ipod/i));
        this.device.android = this.userAgent.match(/android/i);
    }
    
    Cookie.prototype.getCookie = function(name) {
        var index = this.cookieStr.indexOf(name + "=");
        
        if(index == -1) return null;
        
        index = this.cookieStr.indexOf("=", index) + 1;
        var endstr = this.cookieStr.indexOf(";", index);
        
        if(endstr == -1) endstr = this.cookieStr.length;
        
        return unescape(this.cookieStr.substring(index, endstr));
    }
    
    Cookie.prototype.setCookie = function(name, value) {
        if(value != null && value != "")
            d.cookie = name + "=" + escape(value) + "; expires=" + this.expire.toGMTString();
        
        this.cookieStr = d.cookie;
    }
    
    Cookie.prototype.checkSupport = function () {
        this.supportTransition = (supports('transition') ? true : false);
    }
    
    Cookie.prototype.buildHTML = function () {
        var container = d.createElement('div'),
            css = '\
            #cookie-wrapper-animate {\
                width:100% !important;\
                position:fixed !important;\
                left:0 !important;\
                bottom:0 !important;\
                z-index:999999999 !important;\
                height:0;\
                overflow:hidden !important;\
                -webkit-transition:height .4s ease-out;\
                -moz-transition:height .4s ease-out;\
                -ms-transition:height .4s ease-out;\
                -o-transition:height .4s ease-out;\
                transition:height .4s ease-out;\
                background-color:#'+(typeof HashSearch.get('bg') !== 'undefined' ? HashSearch.get('bg') : 'EFEFEF')+' !important;\
            }\
            .cookie-wrapper{\
                border-top:1px solid #CCC !important;\
                width:100% !important;\
                font-family:arial !important;\
                padding:4px 0px !important;\
                line-height: 1.3 !important;\
                '+((this.device.iphone || this.device.android) ? 'font-size:8.5px !important;' : 'font-size:12px !important;')+'\
            }\
            \
            .cookie-wrapper h2,.cookie-wrapper p, .cookie-text {\
                margin:0px 0px 6px 0px !important;\
                font-family:arial !important;\
                color:#'+(typeof HashSearch.get('text') !== 'undefined' ? HashSearch.get('text') : '000000')+' !important;\
            }\
            .cookie-wrapper h2 {\
                font-weight:bold !important;\
                font-size:1.5em !important;\
                height:23px !important;\
            }\
            \
            .cookie-wrapper a{\
                text-decoration:underline !important;\
                color:#'+(typeof HashSearch.get('text') !== 'undefined' ? HashSearch.get('text') : '7A7A7A')+' !important;\
                font-family:arial !important;\
            }\
            .cookie-wrapper-close{\
                display: block !important;\
                position: absolute !important;\
                right: 0px !important;\
                padding: 0px !important;\
                color: #'+(typeof HashSearch.get('text') !== 'undefined' ? HashSearch.get('text') : '666666')+' !important;\
                font-size: 24px !important;\
                width: 24px !important;\
                height: 24px !important;\
                line-height: 24px !important;\
                font-weight: bold !important;\
                cursor: pointer !important;\
                text-align: center !important;\
                text-shadow: rgb(255, 255, 255) 1px 1px 1px !important;\
                font-family:arial !important;\
            }\
            \
            .cookie-content{\
                width:'+(((this.device.android || this.device.iphone) && 500 > w.innerWidth) ? w.innerWidth+'px' : '500px')+' !important;\
                margin:0px auto !important;\
                position: relative !important;\
                font-family:arial !important;\
            }\
            \
            .cookie-text strong{\
                display: block !important;\
                font-family:arial !important;\
            }';
        
        container.innerHTML = '\
        <div id="cookie-wrapper-animate">\
            <div class="cookie-wrapper">\
                <div class="cookie-content">\
                    <span class="cookie-wrapper-close" id="cookie-close">x</span>\
                    <div class="cookie-teaser">\
                        <h2>' + disclaimer_title + '</h2>\
                        <p>' + disclaimer_subtitle + ' <a href="#" id="cookie-teaser-expand">' + disclaimer_subtitle_link + '</a></p>\
                    </div>\
                    <div class="cookie-text">' + disclaimer_msg + '</div>\
                </div>\
            </div>\
        </div>\
        ';
        
        var style = document.createElement('style');
        style.type = 'text/css';
        
        if (style.styleSheet) {
            style.styleSheet.cssText = css;
        } else {
            style.appendChild(document.createTextNode(css));
        }
        
        document.getElementsByTagName('head')[0].appendChild(style);
        
        d.body.appendChild(container);
        
        this.animateCon = d.getElementById('cookie-wrapper-animate');
        
        var button = d.getElementById('cookie-close'),
            thiz = this,
            link = d.getElementById('cookie-teaser-expand');
        
        if(button.addEventListener) {
            button.addEventListener('click', function() {
                thiz.removeOverlay();
            });    
        } else if(button.attachEvent) {
            button.attachEvent('onclick', function() {
                thiz.removeOverlay();
            });
        }
        
        if(link.addEventListener) {
            link.addEventListener('click', function() {
                thiz.showMore();
            });    
        } else if(link.attachEvent) {
            link.attachEvent('onclick', function() {
                thiz.showMore();
            });
        }
    }
    
    Cookie.prototype.showMore = function () {
        if(!this.moreShown) {
            this.moreShown = true;
            
            if(this.supportTransition) {
                this.animateCon.style.height = this.targetHeight+'px';
            } else {
                this.currentStep = 0;
                this.targetSteps = this.targetStepsLast;
                this.renderStep();
            }
        } else {
            this.moreShown = false;
            
            if(this.supportTransition) {
                this.animateCon.style.height = (this.firstStepCount * this.stepCount)+'px';
            } else {
                this.targetSteps = this.targetStepsLast;
                
                this.currentStep = 0;
                this.targetSteps.reverse();
                this.renderStep();
            }
        }
    }
    
    Cookie.prototype.removeOverlay = function () {
        this.setCookie('cookie-allow-site', 1);
        
        if(this.supportTransition) {
            this.animateCon.style.height = '0px';
        } else {
            if(this.moreShown) {
                this.targetSteps = this.overAllStep;
            }
            
            this.currentStep = 0;
            this.targetSteps.reverse();
            this.renderStep();
        }
    }
    
    Cookie.prototype.renderStep = function () {
        if(typeof this.targetSteps[this.currentStep] !== 'undefined') {
            this.animateCon.style.height = this.targetSteps[this.currentStep]+'px';
            
            var thiz = this;
            
            this.currentStep++;
            
            setTimeout(function () {
                thiz.renderStep();
            }, this.speed);
        }
    }
    
    C = new Cookie();
    
    var onready = function(handler) {
        // window is loaded already - just run the handler
        if(d && d.readyState==="complete") return handler();
        
        // non-IE: DOMContentLoaded event
        if(w.addEventListener) w.addEventListener("DOMContentLoaded",handler,false);
        
        // IE top frame: use scroll hack
        else if(w.attachEvent && w==w.top) { if(_readyQueue.push(handler)==1) _readyIEtop(); }
        
        // IE frame: use onload
        else if(w.attachEvent) w.attachEvent("onload",handler);
        
        return true;
    };
    
    // IE stuff
    var _readyQueue = [];
    var _readyIEtop = function() {
        try {
          d.documentElement.doScroll("left");
          var fn; while((fn=_readyQueue.shift())!=undefined) fn();
        }
        catch(err) { setTimeout(_readyIEtop,50); }
    };
    
    onready(function () {
        C.DOMload();
    });
})(document, window);