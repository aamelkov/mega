<?php
function mrmegamod_image_config($form, &$form_state) {
  $form = array();

  /*
   * Games
   */

  $form['games_header'] = array(
    '#weight' => 1,
    '#markup' => '<h3>'.t('Games').'</h3>',
  );

 	$form['games'] = array(
		'#type' => 'vertical_tabs',
		'#title' => t('Games'),
    '#group' => 'vertical_tabs',
    '#weight' => 2,
	);

  $res_games = _mrmegamod_fetch_games();
  $tmp_games = array();

  if (!empty($res_games)) {
    foreach ($res_games as $r_game_nr => $r_game_row) {
      $tmp_games[$r_game_row->field_language_code_value][$r_game_row->entity_id] = $r_game_row->title;
    }
  }

  $language_list = language_list();
  if (!empty($language_list)) {
    $c = 3;
    foreach ($language_list as $l_letters => $l_object) {
      $c++;
      $form['games']['mrmega_settings_lang'.$l_letters] = array(
        '#type' => 'fieldset',
        '#title' => $l_object->name.' ('.$l_letters.')',
        '#weight' => $c,
        '#group' => 'games',
      );

      if (!empty($tmp_games[$l_letters])) {
        $form['games']['mrmega_settings_lang'.$l_letters]['list'] = array(
          '#markup'=>'',
          '#prefix'=>'<table><tr>
            <th style="width:50%;">'.t('Selected games').'</th>
            <th style="width:50%;">'.t('Popular games').'</th>
          </tr><tr>
            <td style="vertical-align:top;padding-bottom:10px;">'.t('Select the games to be dispalyed in the first tab of the game lists').'</td>
            <td style="vertical-align:top;padding-bottom:10px;">'.t('Select the games to be dispalyed in the popular games list, if no games is selected the list wont be displayed').'</td>
          </tr><tr>',
          '#suffix'=>'</tr></table>'
        );

        $form['games']['mrmega_settings_lang'.$l_letters]['list']['mrmega_frontpage_lang'.$l_letters] = array(
          '#type' => 'checkboxes',
          '#title' => t('Selected games'),
          '#options' => $tmp_games[$l_letters],
          '#default_value' => variable_get('mrmega_frontpage_lang'.$l_letters,array()),
          //'#attributes' => array('style' => 'display:table;'),
          '#prefix'=>'<td>',
          '#suffix'=>'</td>',
          '#title_display' => 'invisible',
        );

        $form['games']['mrmega_settings_lang'.$l_letters]['list']['mrmega_popular_lang'.$l_letters] = array(
          '#type' => 'checkboxes',
          '#title' => t('Popular games'),
          '#options' => $tmp_games[$l_letters],
          '#default_value' => variable_get('mrmega_popular_lang'.$l_letters,array()),
          '#prefix'=>'<td>',
          '#suffix'=>'</td>',
          '#title_display' => 'invisible',
        );
      }
      else {
        $form['games']['mrmega_settings_lang'.$l_letters]['message'] = array(
          '#weight' => $c,
          '#markup' => t('There is not added any games with this language'),
          '#weight' => -1,
        );
      }

      $currency = 'EUR';
      $language_code = strtoupper(substr($l_object->native,0,3));
      switch($language_code) {
        case "SWE":
        case "SVE":
          $currency = 'SEK';
          break;
        case "NOR":
          $currency = 'NOK';
          break;
        case "BRA":
          $currency = 'BRL';
          break;
      }

      $form['games']['mrmega_settings_lang'.$l_letters]['mrmega_game_lang'.$l_letters.'_cur'] = array(
        '#type' => 'textfield',
        '#title' => t('Game currency'),
        '#default_value' => variable_get('mrmega_game_lang'.$l_letters.'_cur',$currency),
        '#size' => 4,
        '#attributes' => array('style' => 'width:80px;')
      );
      
      $form['games']['mrmega_settings_lang'.$l_letters]['mrmega_game_lang'.$l_letters.'_bannerimage'] = array(
      	'#title' => t('Banner image'),		
      	'#type' => 'managed_file',
      	'#description' => t('The uploaded image will be displayed on all subpages.'),
      	'#default_value' => variable_get('mrmega_game_lang'.$l_letters.'_bannerimage',''),
      	'#upload_location' => 'public://banners',
      	'#upload_validators' => array(
      		'file_validate_extensions' => array('gif png jpg jpeg'),
      	),	
      );
      
      $form['games']['mrmega_settings_lang'.$l_letters]['mrmega_game_lang'.$l_letters.'_cursymb'] = array(
      	'#type' => 'textfield',
      	'#title' => t('Banner currency symbol'),
      	'#default_value' => variable_get('mrmega_game_lang'.$l_letters.'_cursymb',''),
      	'#size' => 4,
      	'#attributes' => array('style' => 'width:80px;')
      );

      $form['games']['mrmega_settings_lang'.$l_letters]['mrmega_game_lang'.$l_letters.'_code'] = array(
        '#type' => 'textfield',
        '#title' => t('Game language code'),
        '#default_value' => variable_get('mrmega_game_lang'.$l_letters.'_code',$language_code),
        '#size' => 4,
        '#attributes' => array('style' => 'width:80px;')
      );

      $items['element'] = array();
      $c = 0;
      $items['prefix'] = 'mrmega_game_lang'.$l_letters.'_gamesettings';
      $tmp_settings = variable_get($items['prefix'],array());
      if(!empty($tmp_settings)) {
        $tmp_settings = json_decode($tmp_settings, true);
        foreach($tmp_settings as $game_type => $game_info) {
          $items['element'][$game_type] = array(
            'name'=>(!empty($game_info['name']) ? $game_info['name'] : ''),
            'active'=>((!empty($game_info['active']) and $game_info['active']==1) ? TRUE:FALSE),
            'weight'=>$c,
            'game_type' => $game_type,
          );
          $c++;
        }
      }

      $game_types = array('selected-games','slot','casino','games');
      foreach($game_types as $g_nr => $game_type) {
        if(empty($tmp_settings[$game_type])) {
          $items['element'][$game_type] = array('name'=>$game_type,'active'=>1,'weight'=>$c,'game_type'=>$game_type);
          $c++;
        }
      }

      $form['games']['mrmega_settings_lang'.$l_letters]['mrmega_game_lang'.$l_letters.'_table'] = array(
        '#markup' => '<b>'.t('All games tabs').'</b>'.mrmega_form_table_theme_name($items),
      );
    }
  }

  $form['default_settings'] = array(
		'#type' => 'fieldset',
    '#weight' => 90,
    '#title' => t('Default settings'),
    '#group' => 'games',
    '#description' => t('The default settings for page view'),
  );
  $form['default_settings']['contact'] = array(
		'#type' => 'fieldset',
		'#title' => t('Contact info'),
    '#weight' => 50,
	);
  $form['default_settings']['contact']['mrmega_phone'] = array(
		'#type' => 'textfield',
		'#size' => 10,
		'#title' => 'Phone',
		'#default_value' => variable_get('mrmega_phone', ''),
	);

  /*
   * Images
   */

	$form['default_settings']['images'] = array(
		'#type' => 'fieldset',
		'#title' => t('Images'),
    '#weight' => 50,
	);

	$imageNodeID = variable_get('main_image_node');
	$form['default_settings']['images']['image_node'] = array(
		'#type' => 'textfield',
		'#size' => 10,
		'#title' => 'Image node ID',
		'#description' => 'Enter here the node from which you want to fetch the images on <strong>ALL</strong> front pages',
		'#default_value' => (!empty($imageNodeID) ? $imageNodeID : 0),
	);

  /*
   * Frontpage links
   */

	$form['default_settings']['frontpage_links'] = array(
		'#type' => 'fieldset',
		'#title' => t('Frontpage links'),
    '#weight' => 40,
	);

	$form['default_settings']['frontpage_links']['mrmegamod_itech_link'] = array(
		'#type' => 'textfield',
		'#title' => 'iTech Labs',
		'#default_value' => variable_get('mrmegamod_itech_link',''),
	);

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save',
    '#weight' => 99,
	);
	return $form;
}

function mrmegamod_image_config_validate($form, &$form_state){
	if(!is_numeric($form_state['values']['image_node'])) {
		form_set_error('image_node', 'You must enter integer for node ID.');
	}
 	if(!empty($form_state['values']['mrmegamod_gameslist_default_play_now'])) {
		$fid = $form_state['values']['mrmegamod_gameslist_default_play_now'];
		$file = file_load($fid);
		if ($file = file_move($file, 'public://')) {
			$file->status = FILE_STATUS_PERMANENT;
			file_save($file);
			file_usage_add($file, 'mrmega','mrmega_config',1);
		}
		else {
			$form_state['values']['mrmegamod_gameslist_default_play_now'] = 0;
		}
	}

	$language_list = language_list();
	if (!empty($language_list)) {
    foreach ($language_list as $l_letters => $l_object) {
			if(!empty($form_state['values']['mrmega_popular_lang'.$l_letters.'_play_now'])) {
				$fid = $form_state['values']['mrmega_popular_lang'.$l_letters.'_play_now'];
				$file = file_load($fid);
				if ($file = file_move($file, 'public://')) {
					$file->status = FILE_STATUS_PERMANENT;
					file_save($file);
					file_usage_add($file, 'mrmega','mrmega_config',1);
				}
				else {
					$form_state['values']['mrmega_popular_lang'.$l_letters.'_play_now'] = 0;
				}
			}
			
			if(!empty($form_state['values']['mrmega_game_lang'.$l_letters.'_bannerimage'])) {
				$bid = $form_state['values']['mrmega_game_lang'.$l_letters.'_bannerimage'];
				$banner = file_load($bid);
				if ($banner = file_move($banner, 'public://')) {
					$banner->status = FILE_STATUS_PERMANENT;
					file_save($banner);
					file_usage_add($banner, 'mrmega','mrmega_config_banner',1);
				}
				else {
					$form_state['values']['mrmega_game_lang'.$l_letters.'_bannerimage'] = 0;
				}
			}
    }
	}
}

function mrmegamod_image_config_submit($form, &$form_state) {
  if(!empty($form_state['input'])) {
    foreach($form_state['input'] as $field_name => $field_value) {
      if(substr_count($field_name,'_gamesettings')!=0) {
        $form_state['values'][$field_name] = json_encode($field_value);
      }
    }
  }
	system_settings_form_submit($form, $form_state);
}