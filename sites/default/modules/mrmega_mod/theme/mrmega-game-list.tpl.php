<div class="box<?php echo (!empty($details->classes) ? ' '.join(' ', $details->classes) : ''); ?>" id="game<?php echo $details->game_list_id.$details->entity_id; ?>">
<?php if ((isset($details->standard['new_tag']) and $details->standard['new_tag']==1) OR (isset($details->langauge_specefic['new_tag']) and $details->langauge_specefic['new_tag']==1)): ?>
<div class="newGame">&nbsp;</div>
<?php endif; ?>
<div class="box-content">
  <h3 class="box-title"><?php echo $details->title; ?></h3>
  <?php if($details->langauge_specefic['thumb']): ?>
  <?php echo theme('image', array('path' => $details->langauge_specefic['thumb']['uri'])); ?>
  <?php elseif($details->standard['thumb']): ?>
  <?php echo theme('image', array('path' => $details->standard['thumb']['uri'])); ?>
  <?php endif; ?>
  <span class="game-category"><div class="field-content"><?php echo (!empty($details->langauge_specefic['label']) ? $details->langauge_specefic['label'] : $details->standard['label']); ?></div></span>
</div>
<a class="more-link" onclick="<?php echo (!empty($details->standard['link']) ? 'OpenLobby(\''.$details->standard['link'].'\');' : 'return false;'); ?>" href="#">
	<div class="play-now"><?php echo (!empty($details->play_now_bg) ? $details->play_now_bg : '&#160;'); ?></div>
  <div class="field-content"><?php echo (!empty($details->langauge_specefic['button_text']) ? $details->langauge_specefic['button_text'] : $details->standard['button_text']); ?></div>
</a>
