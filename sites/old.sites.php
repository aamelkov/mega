<?php
$sites = array();

/*
 * Default = MrMega
 * mysql: bw10060_db
 */

/*
 * Kronespil
 * mysql: bw10060_kronespil
 */
$sites['tk.kronespil.dev02.accedo.dk'] = 'kronespil';
$sites['mad.kronespil.dev02.accedo.dk'] = 'kronespil';
$sites['lak.kronespil.dev02.accedo.dk'] = 'kronespil';
$sites['jg.kronespil.dev02.accedo.dk'] = 'kronespil';

/*
 * MegaScratch
 * mysql: bw10060_megascratch
 */
$sites['tk.megascratch.dev02.accedo.dk'] = 'megascratch';
$sites['mad.megascratch.dev02.accedo.dk'] = 'megascratch';
$sites['lak.megascratch.dev02.accedo.dk'] = 'megascratch';
$sites['jg.megascratch.dev02.accedo.dk'] = 'megascratch';
